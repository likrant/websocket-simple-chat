package com.li.wssimplechat;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.logging.Logger;

/**
 * Created by IntelliJ IDEA.
 * User: Li
 * Date: 10.05.2015
 * Time: 20:47
 */
@Controller
public class WebController {

    private final Logger log = Logger.getLogger(getClass().getName());


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index() {
        return "index";
    }

}
