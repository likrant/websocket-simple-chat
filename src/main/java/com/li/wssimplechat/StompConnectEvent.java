package com.li.wssimplechat;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.session.web.socket.events.SessionConnectEvent;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * User: Li
 * Date: 11.06.2015
 * Time: 11:41
 */
@Component
public class StompConnectEvent implements ApplicationListener<SessionConnectEvent> {

    private final Log logger = LogFactory.getLog(StompConnectEvent.class);

    public void onApplicationEvent(SessionConnectEvent event) {
        System.out.println("zzz");
//        StompHeaderAccessor sha = StompHeaderAccessor.wrap(event.getMessage());
//
//        String  company = sha.getNativeHeader("company").get(0);
//        logger.debug("Connect event [sessionId: " + sha.getSessionId() +"; company: "+ company + " ]");
    }
}