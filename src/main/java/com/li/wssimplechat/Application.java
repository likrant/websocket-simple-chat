package com.li.wssimplechat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by IntelliJ IDEA.
 * User: Li
 * Date: 10.05.2015
 * Time: 16:30
 */
@Configuration
@ComponentScan
@EnableAutoConfiguration
@EnableConfigurationProperties
public class Application {

    /**
     * Entry point of the application.  Fetches the Runner based on the Spring profile that
     * was used to start the application and runs it.
     */
    public static void main(String[] args) throws Exception {
        ConfigurableApplicationContext app = SpringApplication.run(Application.class, args);
        app.getBean(Runner.class).run();
    }

}