package com.li.wssimplechat;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.socket.WebSocketMessage;

import java.util.Date;

public class ChatMessage implements WebSocketMessage {
	private String message;
	private String sender;
	private Date received;

	public static ChatMessage fromPayload(String payload){
		try {
			return new ObjectMapper().readValue(payload,ChatMessage.class);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return new ChatMessage();
	}


	public final String getMessage() {
		return message;
	}

	public final void setMessage(final String message) {
		this.message = message;
	}

	public final String getSender() {
		return sender;
	}

	public final void setSender(final String sender) {
		this.sender = sender;
	}

	public final Date getReceived() {
		return received;
	}

	public final void setReceived(final Date received) {
		this.received = received;
	}

	@Override
	public String toString() {
		return "ChatMessage [message=" + message + ", sender=" + sender
				+ ", received=" + received + "]";
	}

	@Override
	@JsonIgnore
	public String getPayload() {
		try {
			return new ObjectMapper().writeValueAsString(this);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	@Override
	@JsonIgnore
	public int getPayloadLength() {
		return getPayload().length();
	}

	@Override
	@JsonIgnore
	public boolean isLast() {
		return false;
	}
}
