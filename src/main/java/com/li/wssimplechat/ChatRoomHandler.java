package com.li.wssimplechat;

import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: Li
 * Date: 18.05.2015
 * Time: 13:07
 */
public class ChatRoomHandler extends TextWebSocketHandler {

    @Override
    public void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        ChatMessage chatMessage = ChatMessage.fromPayload(message.getPayload());
        if (chatMessage != null) handleChatMessage(session, chatMessage);
        else super.handleTextMessage(session, message);
    }

    public void handleChatMessage(WebSocketSession session, ChatMessage message) throws Exception {
        if(message.getReceived()==null)message.setReceived(new Date());
        TextMessage returnMessage = new TextMessage(message.getPayload());
        session.sendMessage(returnMessage);
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {

    }

    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
    }

    @Override
    public boolean supportsPartialMessages() {
        return false;
    }
}
