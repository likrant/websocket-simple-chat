package com.li.wssimplechat;

import org.springframework.stereotype.Component;

import java.awt.*;
import java.net.URI;

@Component
public class Runner {


    public void run() {
        String serverPort = "8080";
        launchBrowser("http://localhost:" + serverPort);
    }


    /**
     * Launches the default browser and opens a new tab to the specified URL.
     */
    private static void launchBrowser(String url) {

        // Windows
        if (Desktop.isDesktopSupported()) {
            Desktop desktop = Desktop.getDesktop();
            try {
                desktop.browse(new URI(url));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        // MAC
        else {
            Runtime runtime = Runtime.getRuntime();
            String[] args = {"osascript", "-e", "open location \"" + url + "\""};
            try {
                runtime.exec(args);
            } catch (Exception e) {
                // do what you want with this
            }
        }
    }

}
